import random
import string


words = ["ARSENAL", "CHELSEA", "BARCELONA", "PSG", "TOTTENHAM", "FOOTBALL", "MANCHESTER"]


def display_hangman():
    global number_of_lives 
    number_of_lives -= 1
    stages = [  # final state: head, torso, both arms, and both legs
                """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |     / \\
                   -
                """,
                # head, torso, both arms, and one leg
                """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |     / 
                   -
                """,
                # head, torso, and both arms
                """
                   --------
                   |      |
                   |      O
                   |     \\|/
                   |      |
                   |      
                   -
                """,
                # head, torso, and one arm
                """
                   --------
                   |      |
                   |      O
                   |     \\|
                   |      |
                   |     
                   -
                """,
                # head and torso
                """
                   --------
                   |      |
                   |      O
                   |      |
                   |      |
                   |     
                   -
                """,
                # head
                """
                   --------
                   |      |
                   |      O
                   |    
                   |      
                   |     
                   -
                """,
                # initial empty state
                """
                   --------
                   |      |
                   |      
                   |    
                   |      
                   |     
                   -
                """
    ]
    return stages[number_of_lives]
while True:
    word = random.choice(words)
    user_word = len(word) * "_"
    guessed_letters = []
    special_characters = string.punctuation
    number_of_lives = 7

    while True:
        print(" ".join(user_word))
        try:
            letter = input("Type one letter: ").upper()
            if len(letter) > 1 or letter.isdigit() or letter in special_characters:
                raise ValueError
        except ValueError:
            print("Invalid input, try again")
            continue
        if letter not in word:
            if letter in guessed_letters:
                print("You already guessed the letter!")
                continue
            guessed_letters.append(letter)
            print()
            print(display_hangman())
            if number_of_lives == 0:
                print("Unfortunately, you lose the game! :(")
                break
            else:
                continue
        else:
            if letter in guessed_letters:
                print("You already guessed the letter!")
                continue
            else:
                print(f"Yeah, {letter} is in the word!")
                guessed_letters.append(letter)
                word_as_list = list(user_word)
                indexes = [i for i , character in enumerate(word) if letter == character]
                for index in indexes:
                    word_as_list[index] = letter
                user_word = "".join(word_as_list)
        if "_" not in user_word:
            print("Great! You guessed whole word!")
            break
    play_again = input("If you want to play again, type (Y): ")
    if play_again == "Y":
        continue
